// cloudviewer.cpp : Defines the entry point for the console application.
//

#define IDMLOG_FATAL printf
#define IDMLOG_ERROR printf
#define IDMLOG_WARNING	printf
#define IDMLOG_INFO printf
#define IDMLOG_TRACE printf




#ifdef _WIN32
#pragma message("_WIN32 defined")
#elif defined (WIN32)
#pragma message("win32 defined")
#elif defined(UNIX)
#pragma message("UNIX defined")
#elif defined(__linux__)
#pragma message("__linux__ defined")
#elif defined(__MINGW32__)
#pragma message("__MINGW32__ defined")
#elif defined(__CYGWIN__)
#pragma message("__CYGWIN__ defined")
#endif


//#include "stdafx.h"
#include <GL/glew.h>



//#include "IDMGraphicsTest.h"
//#include "IDMTinyMS.h"
#include "Renderer.h"
#include "RabbitListener.h"
#include <utils/StringUtils.h>

//#include <IDMLogging.h>
//#include "IDMTinyMS.h"



int main(int argc, char* argv[])
{
    if(argc < 3) {
        printf("Usage: cloudviewer <rabbitmq_hostname> <rabbitmq_port> [<pipename>] [tga|yuv] [sequence]\n");
        printf("       Make sure you have RabbitMQ installed with queues cv_cmd_queue and cv_done_queue.\n");
        return 1;
    }

    // host
    char* strHost = argv[1];

    // port
    char* strEnd;
    long port = strtol(argv[2], &strEnd, 10);
    if (*strEnd) {
        IDMLOG_FATAL("[FATAL]: Could not convert argument %s to integer.\n", argv[2]);
        return 1;
    }

    // pipe name
    char* strPipeName = "";
    if(argc >= 4) {
        strPipeName = argv[3];
    }

    idm::cv::OutputType outputType = idm::cv::yuv;
    if(argc >= 5) {
        char* strOutputType = argv[4];
        IDMLOG_INFO("argv[4] = %s\n", argv[4]);
        if(0 == idm::StringUtils::StrCmpI("yuv", strOutputType)){
            outputType = idm::cv::yuv;
        } else if(0 == idm::StringUtils::StrCmpI("tga", strOutputType)){
            outputType = idm::cv::tga;
        } else {
            IDMLOG_FATAL("[WARN]: Argument 4 must be yuv or tga but found %s. Defaulting to yuv.\n", argv[4]);
        }
    }

    bool isSequence = true;
    if(argc >= 6){
        char* strIsSequence = argv[5];
        IDMLOG_INFO("argv[5] = %s\n", argv[5]);
        if(0 == idm::StringUtils::StrCmpI("true", strIsSequence)){
            isSequence = true;
        } else if(0 == idm::StringUtils::StrCmpI("false", strIsSequence)){
            isSequence = false;
        } else {
            IDMLOG_FATAL("[WARN]: Argument 5 must be true or false but found %s. Defaulting to true.\n", argv[5]);
        }
    }
    

    printf("\n");
    printf("Initializing cloudviewer with:\n");
    printf("Hostname: %s\n", strHost);
    printf("Port: %d\n", port);
    printf("PipeName: %s\n", (nullptr!=strPipeName)?strPipeName:"[null, using out/out.yuv]");
    printf("Output type: %s\n", (outputType==idm::cv::yuv)?"yuv":"tga");
    printf("Is sequence type: %s\n", (isSequence)?"true":"false");

	//IDMGraphicsTest test;
	//for (int i = 0; i < 1; i++) {
	//	IDMTinyMS test("assets/MaskFeatAnimAll.idm", IDMTinyMS::TGA);
	//	std::cout << "-------------------" << std::endl;
	//	std::cout << "loading time: " << test.GetLoadingTime() * 1000 << std::endl;
	//	std::cout << "rendering time: " << test.GetRenderingTime() * 1000 << std::endl;
	//	std::cout << "total frames: " << test.GetTotalFrames() << std::endl;
	//	std::cout << "fps: " << 1.0 / (test.GetRenderingTime() / test.GetTotalFrames()) << std::endl;
	//	std::cout << "-------------------" << std::endl;
	//};
	//return 0;

	//if (!INITIALIZE_IDM_LOGGING(idm_log::IDMLogLevel_TRACE, "stdout")){
	//	bool ok = true;
	//}

	// start command processor (rabbit, cmdline, etc)
	// start renderer
	//IDMTinyMS test("assets/tests_Main.idm", IDMTinyMS::TGA);	
	idm::cv::Renderer renderer(strPipeName, outputType, isSequence);

	idm::cv::RabbitListener listener;
	if (!listener.Init(&renderer, strHost, port, "cv_cmd_queue", "cv_done_queue", "engine", "engine", 4))
	{
		IDMLOG_FATAL("[FATAL]: Could not initialize rabbit listener.\n");
		//renderer.Stop();
		return 1;
	}

	if (!renderer.Start()){
		IDMLOG_FATAL("[FATAL]: Could not start renderer.\n");
		listener.CleanUp();
		return 1;
	}

	if (!listener.Start()){
		IDMLOG_FATAL("[FATAL]: Could not start rabbit listener.\n");
		renderer.Stop();
		listener.CleanUp();
		return 1;
	}

	// wait for listener's condition variable
	listener.WaitUntilDone();
	listener.Stop();
	renderer.Stop();
	listener.CleanUp();
	



	//// do nothing for 3 seconds
	//std::this_thread::sleep_for(std::chrono::milliseconds(1000));

	//if (false){
	//	// laod MaskFeatAnimAll.idm
	//	std::string idmPath = std::string("assets/MaskFeatAnimAll.idm");
	//	printf("Pushing command to load idm %s.\n", idmPath.c_str());
	//	idm::cv::CmdLoadIdm* loadCmd = new idm::cv::CmdLoadIdm(&renderer, idmPath);
	//	renderer.SubmitCommand(loadCmd);
	//	std::this_thread::sleep_for(std::chrono::milliseconds(1000));

	//	idm::cv::CmdSetAsset* setCmd = new idm::cv::CmdSetAsset(&renderer, 21, "assets/019.jpg");
	//	renderer.SubmitCommand(setCmd);
	//	setCmd = new idm::cv::CmdSetAsset(&renderer, 22, "assets/019.jpg");
	//	renderer.SubmitCommand(setCmd);
	//	setCmd = new idm::cv::CmdSetAsset(&renderer, 23, "assets/019.jpg");
	//	renderer.SubmitCommand(setCmd);
	//	setCmd = new idm::cv::CmdSetAsset(&renderer, 24, "assets/019.jpg");
	//	renderer.SubmitCommand(setCmd);
	//	setCmd = new idm::cv::CmdSetAsset(&renderer, 25, "assets/019.jpg");
	//	renderer.SubmitCommand(setCmd);

	//	std::this_thread::sleep_for(std::chrono::milliseconds(5000));
	//}
	//else
	//{
	//	// text bbox
	//	std::string idmPath = std::string("assets/TextBBox.idm");
	//	printf("Pushing command to load idm %s.\n", idmPath.c_str());
	//	idm::cv::CmdLoadIdm* loadCmd = new idm::cv::CmdLoadIdm(&renderer, idmPath);
	//	renderer.SubmitCommand(loadCmd);
	//	std::this_thread::sleep_for(std::chrono::milliseconds(100));

	//	idm::cv::CmdSetAsset* setCmd = new idm::cv::CmdSetAsset(&renderer, 7, "Hello hi \n what on earth are you talking about?");
	//	renderer.SubmitCommand(setCmd);
	//	std::this_thread::sleep_for(std::chrono::milliseconds(100));

	//	setCmd = new idm::cv::CmdSetAsset(&renderer, 7, "Hello hi \n what on earth are you talking about\nmother\nfucker?");
	//	renderer.SubmitCommand(setCmd);
	//	std::this_thread::sleep_for(std::chrono::milliseconds(100));

	//	setCmd = new idm::cv::CmdSetAsset(&renderer, 7, "Hello hi \n what on earth are you talking about\nmother\nfucker\nawefawef?");
	//	renderer.SubmitCommand(setCmd);
	//	std::this_thread::sleep_for(std::chrono::milliseconds(100));

	//	setCmd = new idm::cv::CmdSetAsset(&renderer, 7, "Hello hi \n what on earth are you talking about\nmother\nfucker\nawefawef\nsiotjt?");
	//	renderer.SubmitCommand(setCmd);
	//	std::this_thread::sleep_for(std::chrono::milliseconds(100));

	//	setCmd = new idm::cv::CmdSetAsset(&renderer, 7, "Hello hi \n what on earth are you talking about\nmother\nfucker\nawefawef\nsiotjt\nauierfoj?");
	//	renderer.SubmitCommand(setCmd);
	//	std::this_thread::sleep_for(std::chrono::milliseconds(100));
	//}

	//

	//renderer.Stop();
	
	
	return 0;
}

