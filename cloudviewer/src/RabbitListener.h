/*
 *	Author : Eric Yudin (ericy)
 *	All Rights reserved to IDOMOO.INC 2013 - 2017
*/
#pragma once
#ifndef IDM_CV_RABBITLISTENER
#define IDM_CV_RABBITLISTENER


#include <globals/IDMGlobalDefs.h>
#include <amqp.h>
#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>

namespace idm
{
	namespace cv
	{
		class IDMCmdProcessor;
		class Renderer;
		struct Command;

		class RabbitListener
		{
			IDM_DISABLE_COPY(RabbitListener)

		public:
			RabbitListener();
			~RabbitListener();


			/** @brief Init():
			*
			*/
			bool Init(Renderer* renderer, const char* hostname, int port,
				const char* renderQueueName, const char* doneQueueDefaultName,
				const char* userName, const char* passWord,
				int prefetchCount);

			/** @brief CleanUp():
			*
			*/
			void CleanUp();

			/** @brief Start(): starts consuming (consuming will take place on separate thread and this function will return)
			*
			*/
			bool Start();

			/** @brief Stop(): stops the consuming thread
			*
			*/
			bool Stop();

			/** @brief Whether this object has been successfully intialized
			*/
			bool IsInitialized() const;

			/*
			*	
			*/
			void WaitUntilDone();
			std::condition_variable		m_doneCond;
			std::mutex					m_doneMutex;
			std::atomic<bool>			m_quitRequested;

		private:

			/**
			*	Implementation of IIDMRenderRequestSource
			*/

			/** @brief Only call this if you're not calling FinishJob
			*           TODO: replace this sceheme with queue
			*/
			void Finish(uint64_t id, uint32_t errorCode);

			///** @brief Call this when render job is finished
			//*
			//*/
			//virtual void FinishJob(const JobReport& report);

			

			/**
			*	Private member methods
			*/

			// turn the incoming string into a command
			Command* ProcessCommand(const std::string& strCmd);
			bool IsShutdownCommand(const std::string& strCmd);

			/// listener worker. This is what the listener thread runs.
			void Worker();

			/**
			*	Member variables
			*/

			/// indicates that this object is initialized
			bool					m_init;

			/// command processor
			//IDMCmdProcessor*		m_cmd_processor;
			Renderer*				m_renderer;

			/// state information about the connection
			amqp_connection_state_t m_conn_state;

			/// network socket (ultimately winsock)
			amqp_socket_t*			m_socket;

			/// integer "name" we give to a rabbit channel
			amqp_channel_t			m_channel_id;

			/// a string representing the name of the render queue
			amqp_bytes_t			m_render_queue_name;

			/// a string representing the name of the done queue
			amqp_bytes_t			m_done_queue_name;

			/// a string representing the name of the default done queue
			amqp_bytes_t			m_done_queue_default_name;

			/// a string representing who this consumer is
			amqp_bytes_t			m_consumer_tag;

			/// this listener will run on its own thread
			std::thread				m_listener_thread;

			/// flag to indicate whether to continue working
			std::atomic<bool>		m_done;

			/// flag to indicate whether to continue working
			std::mutex				m_mutex;
		};


		extern bool return_on_amqp_error(amqp_rpc_reply_t x, char const *context);
		extern bool return_on_error(int x, char const *context);
	}

}


#endif