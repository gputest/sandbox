#pragma once
#ifndef IDM_CV_ERROR_H
#define IDM_CV_ERROR_H


typedef enum
{
	CvNoErrors,				//0
	//CvFailedToOpenIDMFile,		//1
	//CvFailedToOpenJsonFile,	//2
	//CvIDMParsingError,			//3
	//CvJsonParsingError,		//4
	//CvUserSceneIdError,		//5
	//CvUserPlaceHolderIdError,	//6
	//CvUserMissingAttribute,	//7
	//CvAssetLoadingError,		//8
	//CvFailedToOpenOutFile,		//9
	//CvInvalidCommand,			//10
	//CvInvalidPath,				//11
	CvNetworkConnectionError//,	//12
	//CvInternalError,			//13
	//CvNVENCError,				//14
	//CvMuxerError               //15
} ECvErrorCode;

// Supported tag ID/Message  pair
typedef struct tagCvErrorString {
	ECvErrorCode ErrorCode;
	const char*  Message;
} TCvErrortring, *PCvErrorString;
// Table representation of supported tags (ID, Name)
static const TCvErrortring CvErrorMappingTable[] =
{
	{ CvNoErrors, "No Errors " },
	//{ CvFailedToOpenIDMFile, "Failed To Open IDM File " },
	//{ CvFailedToOpenJsonFile, "Failed To Open Json File " },
	//{ CvIDMParsingError, "IDM Parsing Error " },
	//{ CvJsonParsingError, "Json Parsing Error " },
	//{ CvUserSceneIdError, "User SceneId Error " },
	//{ CvUserPlaceHolderIdError, "User PlaceHolder Id Error " },
	//{ CvUserMissingAttribute, "User Missing Attribute " },
	//{ CvAssetLoadingError, "Asset Loading Error " },
	//{ CvFailedToOpenOutFile, "Failed To Open Output File " },
	//{ CvInvalidCommand, "Invalid Command " },
	//{ CvInvalidPath, "Invalid Path " },
	{ CvNetworkConnectionError, "Network Connection Error " }//,
	//{ CvInternalError, "Internal Error " },
	//{ CvNVENCError, "NVENC Error " }
};

#define CvErrorMappingTable_SIZE (sizeof(CvErrorMappingTable)/sizeof(TCvErrortring))


#endif